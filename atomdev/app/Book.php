<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    public $primaryKey = 'id';

    public $timeStamps = true;

    // N Books - 1 User 
    public function user() {
        return $this->belongsTo('App\User');
    }
}
