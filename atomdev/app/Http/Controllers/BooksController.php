<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BooksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('created_at', 'desc')->paginate(2);
        return view('books.index')->with('books', $books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'isbn' => 'required',
            'year' => 'required',
            'description' => 'required'
        ]);

        $book = new Book;
        $book->name = $request->input('name');
        $book->isbn = $request->input('isbn');
        $book->year = $request->input('year');
        $book->description = $request->input('description');
        $book->user_id = auth()->user()->id;
        $book->cover_img = 'https://99designs-blog.imgix.net/wp-content/uploads/2017/12/attachment_83090027.jpg?auto=format&q=60&fit=max&w=930';
        $book->save();    

        return redirect('/books')->with('success', 'Book added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);
        return view('books.show')->with('book', $book);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);

        // Check for updating someone else bo
        if (auth()->user()->id !== $book->user_id) {
            return redirect('/books')->with('error','No permissions');
        }

        return view('books.edit')->with('book', $book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'isbn' => 'required',
            'year' => 'required',
            'description' => 'required'
        ]);

        $book = Book::find($id);
        $book->name = $request->input('name');
        $book->isbn = $request->input('isbn');
        $book->year = $request->input('year');
        $book->description = $request->input('description');
        $book->cover_img = 'https://99designs-blog.imgix.net/wp-content/uploads/2017/12/attachment_83090027.jpg?auto=format&q=60&fit=max&w=930';
        $book->save();    

        return redirect('/books')->with('success', 'Book updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        // Check for updating someone else bo
        if (auth()->user()->id !== $book->user_id) {
            return redirect('/books')->with('error','No permissions');
        }
        
        $book->delete();
        
        return redirect('/books')->with('success', 'Book removed');
    }

}
