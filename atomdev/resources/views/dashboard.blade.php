@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 style="float: left">My books</h3>
                    <a href="/books/create/" class="btn btn-primary" style="float:right">Create book</a>
                </div>


                



                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4>Your books</h4>
                    @if (count($books) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach ($books as $book)
                                <tr>
                                    <td>{{$book->name}}</td>
                                    <td>
                                        <a href="/books/{{$book->id}}/edit" class="btn btn-default">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        {!!Form::open(['action' => ['BooksController@destroy', $book->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                        {!!Form::close()!!}
                                    </td>
                                </tr>
                            @endforeach
        

                        </table>
                    @else
                        You have no added books
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
