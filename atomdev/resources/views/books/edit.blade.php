@extends('layouts.app')

@section('content')

    <h1>Edit book #{{$book->id}}</h1>

    {{-- https://laravelcollective.com/docs/master/html --}}

    {!! Form::open(['action' => ['BooksController@update', $book->id], 'method' => 'POST']) !!}
        <div class="form-group">

            {{Form::label('name', 'Name')}}
            {{Form::text('name', $book->name, ['class' => 'form-control'])}}
            <br>
            {{Form::label('isbn', 'Isbn')}}
            {{Form::text('isbn', $book->isbn, ['class' => 'form-control'])}}
            <br>
            {{Form::label('year', 'Year')}}
            {{Form::number('year', $book->year, ['class' => 'form-control'])}}
            <br>
            {{Form::label('description', 'Description')}}
            {{Form::textarea('description', $book->description, ['class' => 'form-control'])}}
            <br>
            {{Form::hidden('_method', 'PUT')}}
            {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

            {{-- // TODO --}}
            {{-- {{Form::file($name, $attributes = []);}} --}}
            

        </div>
    {!! Form::close() !!}

@endsection
    
