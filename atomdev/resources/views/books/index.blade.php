@extends('layouts.app')

@section('content')

    @if(count($books) > 0)
        @foreach ($books as $book)

            <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                    <h3 class="mb-0">
                        <a class="text-dark" href="/books/{{$book->id}}">
                            {{$book->name}}
                        </a>
                    </h3>
                    
                    <div class="mb-1 text-muted">
                        {{$book->year}}
                    </div>
                    
                    <p class="card-text mb-auto">
                        {{$book->description}}
                    </p>
                    
                    <div class="mb-1 text-muted">
                        Added by: {{$book->user->name}}
                    </div>

                    <a href="/books/{{$book->id}}">
                        View more >
                    </a>

                    {{-- <a href="/books/{{$book->id}}/fav">
                        Add to Favourite
                    </a> --}}

                </div>
                <img class="card-img-right flex-auto d-none d-lg-block" data-src="{{$book->cover_img}}" alt="Thumbnail [200x250]" style="width: 200px; height: 250px;" src="{{$book->cover_img}}" data-holder-rendered="true">
            </div>

        @endforeach
        {{$books->links()}}
    @else
        No books found
    @endif

@endsection



