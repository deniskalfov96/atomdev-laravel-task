@extends('layouts.app')

@section('content')

    <h1>Create book</h1>

    {{-- https://laravelcollective.com/docs/master/html --}}

    {!! Form::open(['action' => 'BooksController@store', 'method' => 'POST']) !!}
        <div class="form-group">

            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control'])}}
            <br>
            {{Form::label('isbn', 'Isbn')}}
            {{Form::text('isbn', '', ['class' => 'form-control'])}}
            <br>
            {{Form::label('year', 'Year')}}
            {{Form::number('year', '', ['class' => 'form-control'])}}
            <br>
            {{Form::label('description', 'Description')}}
            {{Form::textarea('description', '', ['class' => 'form-control'])}}
            <br>
            {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

            {{-- // TODO --}}
            {{-- {{Form::file($name, $attributes = []);}} --}}
            

        </div>
    {!! Form::close() !!}

@endsection
    
