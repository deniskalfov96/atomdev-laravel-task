@extends('layouts.app')

@section('content')

            <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                    <h3 class="mb-0">
                        <a class="text-dark" href="/books/{{$book->id}}">
                            {{$book->name}}
                        </a>
                    </h3>
                    
                    <div class="mb-1 text-muted">
                        {{$book->year}}
                    </div>
                    
                    <p class="card-text mb-auto">
                        {{$book->description}}
                    </p>
                    
                    <div class="mb-1 text-muted">
                        Added by: {{$book->user->name}}
                    </div>

                    {{-- <a href="/books/{{$book->id}}">
                        View more >
                    </a> --}}
                    <br><br>

                    <a href="/books">< Go back</a>

                    {{-- @if(!Auth::guest()) --}}
                    @if(auth()->user()->id == $book->user->id)
                    
                    {!!Form::open(['action' => ['BooksController@destroy', $book->id], 'method' => 'POST', 'class' => 'pull-right', 'style' => 'width:100%'])!!}
                        {{Form::hidden('_method', 'DELETE')}}
                        <a href="/books/{{$book->id}}/edit" class="btn btn-default" style="float:left">Edit</a>
                        {{Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'float:right'])}}
                    {!!Form::close()!!}
                    @endif

                </div>
                <img class="card-img-right flex-auto d-none d-lg-block" data-src="{{$book->cover_img}}" alt="Thumbnail [200x250]" style="width: 200px; height: 250px;" src="{{$book->cover_img}}" data-holder-rendered="true">
            </div>


@endsection



